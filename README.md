# helloNode
我们使用HBuilderX创建一个web项目，取名为“helloNode”。
## 目录结构
* helloNode 
 + js
	- main.js 
	- module.js
 - index.html	

 ## 文件代码
 

  **main.js**
 ```javascript
 var module = require('./module') // 引用模块，可省略 .js 后缀名
module.sum(); // 调用 module.js 中的函数
 ```
**module.js**
 ```javascript
 // node 模块化编程, 导出函数
module.exports = {
	param1:28655,
	param2:37433,
	sum:function(){
		var result = this.param1+this.param2;
		console.log('我是一个js模块,我可以秒算：'+this.param1+'+'+this.param1+'='+result+'。');
	}
}
 ```
  **index.html**
 ```html
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Demo</title>
	</head>
	<body>
		<div>
			<span>第一个node项目!</span>
		</div>
		<script src="./js/main.js"></script>
	</body>
</html>
 ```
## 访问页面
在HBuilderX中，点击右上角的预览，可以把这个项目使用老方法发布到局域网中。此时访问index.html发现页面内容正常，但是F12可见main.js中出现报错：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200806181056460.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1MTM0NjA=,size_16,color_FFFFFF,t_70)
当然不行了，毕竟我们使用了node的特性。

## node运行试试
在cmd中进入项目所在路径，使用node关键字调用js文件：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200806180624525.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1MTM0NjA=,size_16,color_FFFFFF,t_70)
可以看到，main.js居然正常运行并打印了数据。
爽歪歪，好神奇！

## webpack打包
刚刚我们试了node项目无法正常部署访问，那有个毛用？
马上，引入重点。
我们需要使用webpack对node项目进行打包编译，将相关js代码转换为普通浏览可以执行的代码。
在cmd中，使用webpack 关键字，将main.js编译为bundle.js。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200806181634132.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1MTM0NjA=,size_16,color_FFFFFF,t_70)
编译完成后，我们的js目录下就多个了个bundle.js文件，它就是浏览器正常运行的脚本了。

此时，我们将index.html中引用main.js的地方改为bundle.js；再此使用浏览器访问index.html时：
![在这里插入图片描述](https://img-blog.csdnimg.cn/202008061819547.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1MTM0NjA=,size_16,color_FFFFFF,t_70)
页面正常，脚步运行也正常！
至此，我们的前端项目结构将发生重大变化。
原来混乱的脚本，将被一个一个模块规范的管理起来。
既方便了管理维护，又方便了代码互用。